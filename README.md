Münchner Physik-Kolloquium
==========================

Organisation des Münchner Physik-Kolloquiums 

Die Vorträge für das kommende Semester finden sich in der entsprechenden Datei 
für das Semester. 

**Achtung: Nach dem Editieren muss unten auf das grüne "Commit changes" gedrückt werden, um die Datei zu speichern!!!**

Aktuell ist: 
[kolloquium-2018-Winter.txt](https://gitlab.lrz.de/jowi/kolloquium/blob/programmplanung/kolloquium-2018-Winter.txt)

Die Felder sind in der Mustervorlage erklärt: 
[kolloquium-vorlage.txt](https://gitlab.lrz.de/jowi/kolloquium/blob/programmplanung/kolloquium-vorlage.txt)

Das fertige Programm findet sich unter: https://www.ph.tum.de/kolloquium/

Lokaler Speicherort `jowi-lap`: 

/home/jowi/nas/webrelaunch/source/intern/wwwph/latest/events/colloquium/kolloquium-2018-Winter.txt
