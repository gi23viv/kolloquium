:Date:         2018-04-09
:Title:        Nuclear physics as precision science
:Speaker:      Prof. Dr. Ulf-G. Meißner
:Gender:       male
:Affiliation:  \mbox{Universität Bonn} and \mbox{Forschungszentrum Jülich}
:Location:     TUM
:Abstract: 
Theoretical nuclear physics has entered a new era.  Using the powerful machinery 
of chiral effective Lagrangians, the forces between two, three and four nucleons 
can now be calculated with unprecedented precision and with reliable 
uncertainties.  Furthermore, Monte Carlo methods can be adopted to serve as a 
new and powerful approach to exactly solve nuclear structure and reactions.  I 
discuss the foundations of these new methods and provide a variety of intriguing 
examples.  Variations of the fundamental constants of Nature can also be 
investigated and the consequences for the element generation in the Big Bang and 
in stars are considered.  This sheds new light on our anthropic view of the 
Universe. 
:Endabstract:

:Date:         2018-04-16
:Title:        Probing the unfolding/refolding dynamics of individual proteins with AFM by leveraging enhanced spatio-temporal resolution
:Speaker:      Dr. Devin Edwards
:Gender:       male
:Affiliation:  JILA, National Institute of Standards and Technology and University of Colorado, Boulder, USA
:Location:     LMU
:Abstract: 
Single-molecule force spectroscopy (SMFS) is an important tool for
characterizing the unfolding/refolding dynamics of individual molecules.
Here, we apply custom-modified atomic force microscopy (AFM) cantilevers
to repeatedly unfold and refold the protein $\alpha$3D at equilibrium.
Observing a single protein unfold and refold hundreds of times enables
the discovery of rarely populated intermediates and the reconstruction
of the 1-D energy landscape. However, such equilibrium experiments have
traditionally been inaccessible with AFM-based SMFS due to limited
long-term stability. To overcome these limitations, we developed
focused-ion-beam modified AFM cantilevers that achieve an unparalleled
combination of force stability, force precision, and temporal
resolution. This enhanced data quality allowed identification of an
unfolding intermediate and transition path times that were previously
undetectable with AFM.
:Endabstract:

:Date:         2018-04-23
:Title:        The response of the global stratospheric circulation to climate change
:Speaker:      Prof. Dr. Hella Garny
:Gender:       female
:Affiliation:  \mbox{Meteorologisches Institut}, Ludwig-Maximilians-Universität München
:Location:     TUM
:Abstract: 
The role of the stratosphere in the climate system is increasingly being 
appreciated, and it is known that the circulation of the stratosphere can 
significantly influence surface climate and weather. The fate of the large-scale 
circulation of the stratosphere in a changing climate is a much discussed topic 
in the last years. Progress has been made on the understanding of the mechanisms 
of the general acceleration of the circulation in response to climate change as 
simulated by models. However, observational evidence on circulation changes is 
still not reconciled with model simulations and with our mechanistic 
understanding. The key open questions on large-scale circulation changes and 
their possible impacts on the climate system that will be discussed during this 
talk are (1) Process understanding: How is tracer transport (that is detectable 
from observations) coupled to the wave-driven mean mass circulation, the 
residual circulation? (2) How large is the uncertainty in the projections due to 
sub-grid scale parametrized wave forcing of the circulation? (3) How certain are 
we on the deduction of observational measures of stratospheric transport times?
:Endabstract:

:Date:         2018-04-30
:Title:        Shedding light on the dark cosmos through gravitational lensing  
:Speaker:      Prof. Dr. Sherry Suyu  
:Gender:       female
:Affiliation:  \mbox{Technische Universität München} and \mbox{MPI für Astrophysik, Garching}
:Location:     LMU  
:Abstract: 
Strong gravitational lenses with measured time delays between the multiple 
images can be used to determine the Hubble constant that sets the expansion rate 
of the Universe. Measuring the Hubble constant is crucial for inferring 
properties of dark energy, spatial curvature of the Universe and neutrino 
physics.  I will describe techniques for measuring the Hubble constant from 
lensing with a realistic account of systematic uncertainties.  A program 
initiated to measure the Hubble constant to <~3.5\,\% in precision from strong 
lenses is in progress.  Search is underway to find new lenses in imaging 
surveys.  An exciting discovery of the first strongly lensed supernova offered a 
rare opportunity to perform a true blind test of our modeling techniques. I will 
show the bright prospects of gravitational lens time delays as an independent 
and competitive cosmological probe.
:Endabstract:

:Date:         2018-05-07
:Title:        Topological photonics
:Speaker:      Prof. Dr. Alexander Szameit
:Gender:       male
:Affiliation:  Universität Rostock
:Location:     TUM  
:Abstract: 
The recent experiments on photonic topological insulators signified a new 
direction. We present the progress in this area, including also the first 
observation of topological Anderson insulators, with an emphasis on universal 
ideas common to optics, cold atoms and quantum systems.
:Endabstract:

The discovery of topological insulators relying on spin-orbit coupling in 
condensed matter systems has created much interest in various fields, including 
in photonics. In two-dimensional electronic systems, topological insulators are 
insulating materials in the bulk, but conduct electric current on their edges 
such that the current is completely immune to scattering. However, demonstrating 
such effects in optics poses a major challenge because photons are bosons, which 
fundamentally do not exhibit fermionic spin-orbit interactions (i.e., Kramer’s 
theorem). At microwave frequencies, topological insulators have been [1] in 
magneto-optic materials, relying on strong magnetic response to provide 
topological protection against backscattering – in the spirit of the quantum 
Hall effect. However, at optical frequencies the magneto-optic response is 
extremely weak, hence a photonic topological insulator would have to rely on 
some other property. Indeed, numerous theoretical proposals have been made for 
photonic topological insulators [2], but their first observation [3], made 
by our group, relied on a different idea: Floquet topological insulators [4]. 
Later that year, another group reported imaging of topological edge states in 
silicon photonics [5]. These experiments have generated much follow up, among 
them – as the arguably most intriguing one the area of “topological photonics” – 
our first experimental observation of topological Anderson insulators (predicted 
in [6]], where a system becomes topological only when disorder is introduced 
[7]. The purpose of this talk is to review these and other developments, discuss 
new conceptual ideas, and suggest applications.

REFERENCES
[1]	Nature 461, 772 (2009)
[2]	Phys. Rev. A 82, 043811 (2010); Phys. Rev. A 84, 043804 (2011); Nature Phys. 7, 907 (2011); Nature Photon. 6, 782 (2012); Nature Mater. 12, 233 (2013).
[3]	Nature 496, 196 (2013).
[4]	Phys. Rev. B 82, 235114 (2010); Nature Phys. 7, 490 (2011). 
[5] 	Nature Photon. 7, 1001 (2013); Phys. Rev. Lett. 113, 087403 (2014).
[6] 	Phys. Rev. Lett. 102, 136806 (2009); Phys. Rev. Lett. 114, 056801 (2015).
[7]	CLEO/QELS conference, paper FTh3D.2, San José, USA (2015).

:Date:         2018-05-14
:Title:        Artificial photosynthesis with functional semiconductors and nanosystems
:Speaker:      Prof. Dr. Ian D. Sharp
:Gender:       male
:Affiliation:  Walter-Schottky-Institut and Physik-Department, Technische Universität München
:Location:     LMU
:Abstract: 
The capture of sunlight and its direct conversion to chemical
fuels in artificial photosystems provides a promising route for
sustainably meeting future energy demands. While functional systems
comprising semiconductors coupled to catalysts have been demonstrated,
progress has been hindered by a lack of materials that are both stable
and efficient. Here, opportunities for creating new functionality via
nanoscale engineering of semiconductor/catalyst interfaces, the
development of new transition metal-based semiconductors, and discovery
of basic mechanisms of energy conversion and efficiency loss will be
discussed.
:Endabstract:

:Date:         2018-05-28
:Title:        Emil J. Gumbel: Weimar pacifist and founder of the statistical theory of extremes
:Speaker:      Prof. Dr. Matthias Scherer
:Gender:       male
:Affiliation:  \mbox{Fakultät für Mathematik, Technische Universität München}
:Location:     TUM  
:Abstract: 
GUMBEL (\textborn 1891 Munich; \textdied 1966 New York). Eponym in mathematical statistics for the first type extreme value 
distribution and the copula that is both of extreme value and Archimedean kind. 
Hydrologists appreciate Emil J. Gumbel as a pioneer in promoting non-normal 
distributions in their field. Historians rank him among the most influential 
German intellectuals of the Weimar Republic. He disclosed secret societies that 
destabilized the Weimar Republic and used statistical methods to document 
political murders and to reveal a biased legal system. Born in Munich, he later 
became the first professor who lost his position for his political ideals and 
his stand against the national socialistic party, his books were banned and 
burned. Stripped of his nationality in 1933 he immigrated to France. In 1940 he 
escaped to the USA and settled in New York, where he was appointed Adjunct 
Professor at Columbia University in 1953. With this talk we commemorate the 
political life and the scientific contribution of the Munich-born mathematician 
Emil J. Gumbel and we recall the early years of the statistical theory of 
extremes.
:Endabstract:

:Date:         2018-06-04
:Title:        High-contrast observations of extrasolar planets and protoplanetary disks  
:Speaker:      Dr. Sascha Quanz  
:Gender:       male
:Affiliation:  ETH Zürich, Switzerland
:Location:     LMU  
:Abstract: 
All major ground-based astronomical observatories are equipped with 
sophisticated, adaptive optics assisted high-contrast imaging instruments 
working at optical and/or near-infrared wavelengths.  One of the key science 
drivers for these developments is the direct detection of extrasolar planets and 
circumstellar disks, the birthplaces of planetary systems.  In this talk I will 
motivate the scientific interest for this research field, explain the main 
challenges for high-contrast imaging observations and how they can be overcome, 
and give a broad overview about the key scientific results obtained over the 
last years.  I will conclude with an outlook what we can expect in the future 
with high-contrast imaging instruments for the next generation of 30-40 m 
telescopes such as Europe’s  Extremely Large Telescope. 
:Endabstract:

:Date:         2018-06-11
:Title:        When physics meets medicine: Targeted radionuclide therapy of cancer for precision oncology  
:Speaker:      Prof. Dr. Richard Baum 
:Gender:       male
:Affiliation:  Zentralklinik Bad Berka
:Location:     TUM  
:Abstract: 
Precision medicine is defined as treatments targeted to the needs of
individual patients on the basis of genetic, biomarker, phenotypic, or
psychosocial characteristics that distinguish a given patient from other
patients with similar clinical presentations. Inherent in this
definition is the goal of improving clinical outcomes for individual
patients and minimizing unnecessary side effects for those less likely
to have a response to a particular treatment.

Over the past decade, the use of Gallium-68 labeled somatostatin
receptor (SSTR) PET/CT imaging followed by Lutetium-177 labeled
SSTR-agonist (DOTATATE or DOTATOC) for peptide receptor radionuclide
therapy (PRRT) has demonstrated remarkable success in the management of
neuroendocrine neoplasms. Highly promising advances are being made in
the management of advanced stage, progressive treatment refractory
prostate cancer by applying the principle of theranostics (integration
of diagnostics and therapeutics in the individualized management of
disease). 

Rapid progress is being made in the development of several radiometals
potentially useful for theranostics. Matched pairs of radionuclides are
being developed from the same element with comparable half-lives, to
allow preparation of chemically identical radiopharmaceuticals for
diagnostic and therapeutic purposes. 
:Endabstract:

:Date:         2018-06-18
:Title:        From dust storms on Mars to streaming in protoplanetary disks  
:Speaker:      Prof. Dr. Gerhard Wurm  
:Gender:       male
:Affiliation:  Universität Duisburg-Essen
:Location:     LMU  
:Abstract: 
On Mars dust storms can rage the whole planet and dust devils frequently cross its barren land. The physics is richer though as gas no longer follows the rules we know so well at Mars' low ambient pressure. Gas can creep along surfaces from cold to warm sides, somewhat counterintuitively. 

Different story, some not so ordinary interaction between gas and solids might also be important in protoplanetary disks. Here, so called streaming instabilities can concentrate solids to become larger bodies, eventually.

These effects can be studied in laboratory experiments, on ground as well as under microgravity. 
:Endabstract:

:Date:         2018-06-25
:Title:        High-temperature superconductivity: new insights and perspectives
:Speaker:      Prof. Dr. Bernhard Keimer  
:Gender:       male
:Affiliation:  Max-Planck-Institut für Festkörperforschung, Stuttgart
:Location:     TUM
:Abstract: 
Three decades after the discovery of high-temperature superconductivity, 
experimental advances yield surprising new insights [1]. Beginning with an 
elementary introduction to superconductivity, this colloquium will outline our 
current understanding of this phenomenon. We will then discuss the latest 
developments, with a focus on electronic collective modes detected by 
high-resolution neutron and x-ray spectroscopies, and on the discovery of charge 
order and its interplay with superconductivity. We will also discuss 
perspectives for controlled manipulation of high-temperature superconductors and 
other correlated-electron materials at interfaces and in electronic devices.

[1] For recent reviews, see B. Keimer et al., Nature 518, 179 (2015); B. Keimer 
and J.E. Moore, Nature Physics 13, 1045 (2017).
:Endabstract:

:Date:         2018-07-02
:Title:        Deep X: Deep learning with deep knowledge
:Speaker:      Prof. Dr. Volker Tresp
:Gender:       male
:Affiliation:  Ludwig-Maximilians-Universität München and Siemens
:Location:     LMU  
:Abstract: 
We argue that a labeled graph is an appropriate description of world
state and world events on a cognitive abstraction level, representing
facts as subject-predicate-object triples. A prominent and very
successful example is the Google Knowledge Graph, representing on the
order of 100B facts. Labeled graphs can be represented as adjacency
tensors which can serve as inputs for prediction and decision-making,
and from which tensor models can be derived to generalize to unseen
facts. We show how these ideas can be used, together with deep recurrent
networks, for clinical decision support by predicting orders and
outcomes. Following Goethe’s proverb, ``you only see what you know'', we
show how background knowledge can dramatically improve information
extraction from images by deep convolutional networks and how tensor
train models can be used for the efficient classification of videos. We
discuss potential links to the memory and perceptual systems of the
human brain. We further propose quantum algorithms to solve
high-dimensional tensor decomposition problems, which could be
implemented on a D-Wave quantum annealer or noisy intermediate-scale
quantum computers. We conclude that tensor models, in connection with
deep learning, can be the basis for many technical solutions requiring
memory and perception and might bridge modern physics and modern AI.
:Endabstract:

:Date:         2018-07-09
:Title:        PicoPhotonics: Extreme nano-optics with single molecules and monolayers
:Speaker:      Prof. Dr. Jeremy F. Baumberg
:Gender:       male
:Affiliation:  \mbox{Cavendish Laboratory, University of Cambridge}, UK
:Location:     TUM
:Abstract: 
Coupling between coinage metal `plasmonic' nano-components generates strongly 
red-shifted optical resonances combined with intense local light amplification 
on the nanoscale. I will show how we now create ultralow volume plasmonic 
cavities trapping light to <~1\,nm$^3$, and are routinely able to watch individual 
molecules and bonds vibrating. Using DNA origami we couple 1--4 dye molecules 
together optomechanically, and produce strong-light matter coupling that changes 
their quantum emission properties. We also watch redox chemistry in real time, 
watching single electrons shuttle in and out of single molecules, as well as 2D 
materials confined in the same gap. Prospective applications range from 
(bio)molecular sensing to fundamental science.
:Endabstract:

